package com.nk.rpg.input;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.HashMap;

import com.nk.rpg.RPG;

public class Input implements KeyListener {
	
	public boolean[] downkeys = new boolean[65536];
	public boolean[] typedkeys = new boolean[65536];
	public boolean[] togglekeys = new boolean[65536];
	public boolean[] busykeys = new boolean[65536];
	
	public HashMap<String, Integer> controls = new HashMap<String, Integer>();
	
	public int lastPressedKey = 0;
	
	public Input(RPG game) {
		game.addKeyListener(this);
		
		controls.put("up", KeyEvent.VK_UP);
		controls.put("left", KeyEvent.VK_LEFT);
		controls.put("down", KeyEvent.VK_DOWN);
		controls.put("right", KeyEvent.VK_RIGHT);
		controls.put("escape", KeyEvent.VK_ESCAPE);
	}
	
	public void tick() {
		for (int i = 0; i < 65536; i++) {
			if (togglekeys[i]) {
				togglekeys[i] = false;
				busykeys[i] = true;
			} else if (downkeys[i] && !togglekeys[i] && !busykeys[i]) {
				togglekeys[i] = true;
			}
			
			if (busykeys[i]) {
				if (!downkeys[i]) {
					busykeys[i] = false;
				}
			}
		}
	}
	
	@Override
	public void keyPressed(KeyEvent ke) {
		lastPressedKey = ke.getKeyCode();
		downkeys[ke.getKeyCode()] = true;
	}

	@Override
	public void keyReleased(KeyEvent ke) {
		downkeys[ke.getKeyCode()] = false;
	}

	@Override
	public void keyTyped(KeyEvent ke) {
		typedkeys[ke.getKeyCode()] = true;
	}
}
