package com.nk.rpg;

import java.awt.BorderLayout;
import java.awt.Canvas;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.image.BufferStrategy;
import java.awt.image.BufferedImage;
import java.util.Random;

import javax.swing.JFrame;

import com.nk.rpg.gfx.Screen;
import com.nk.rpg.input.Input;

public class RPG extends Canvas implements Runnable {

	private static final long serialVersionUID = 1L;
	private static JFrame frame;
	public static int WIDTH = 240;
	public static int HEIGHT = 176; // 720 x 528, 15 x 11
	public static int SCALE = 3;
	public static final String TITLE = "RPG";

	public static void main(String[] args) {
		RPG game = new RPG();
		game.setMinimumSize(new Dimension(WIDTH * SCALE, HEIGHT * SCALE));
		game.setMaximumSize(new Dimension(WIDTH * SCALE, HEIGHT * SCALE));
		game.setPreferredSize(new Dimension(WIDTH * SCALE, HEIGHT * SCALE));

		frame = new JFrame(TITLE);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setLayout(new BorderLayout());
		frame.add(game);
		frame.pack();
		frame.setResizable(false);
		frame.setLocationRelativeTo(null);
		frame.setVisible(true);
		frame.setIgnoreRepaint(true);
		game.start();
	}

	private boolean running = false;
	private boolean paused = false;

	private BufferedImage image = new BufferedImage(WIDTH, HEIGHT, BufferedImage.TYPE_INT_RGB);
	private Input input;
	private Screen screen;
	private Game game;

	public static Random r = new Random();

	public void init() {
		input = new Input(this);
		screen = new Screen(WIDTH, HEIGHT, image.getGraphics());
		game = new Game(this, input, screen, 15, 11);
	}

	public void start() {
		requestFocus();
		running = true;
		new Thread(this).start();
	}

	public void stop() {
		running = false;
	}

	@Override
	public void run() {
		createBufferStrategy(2);
		BufferStrategy bs = getBufferStrategy();

		init();

		while (running) {
			final double GAME_HERTZ = 60.0;
			final double TIME_BETWEEN_UPDATES = 1000000000 / GAME_HERTZ;
			final int MAX_UPDATES_BEFORE_RENDER = 1;
			double lastUpdateTime = System.nanoTime();

			while (running) {
				double now = System.nanoTime();
				int updateCount = 0;

				if (!paused) {
					while (now - lastUpdateTime > TIME_BETWEEN_UPDATES && updateCount < MAX_UPDATES_BEFORE_RENDER) {
						tick();
						lastUpdateTime += TIME_BETWEEN_UPDATES;
						updateCount++;
					}

					render(bs);

					Thread.yield();
					now = System.nanoTime();
				}
			}
		}

		System.exit(0);
	}

	private void tick() {
		game.tick();
	}

	private void render(BufferStrategy bs) {
		do {
			do {
				Graphics g = image.getGraphics();
				g.clearRect(0, 0, WIDTH, HEIGHT);

				game.draw(screen);

				g = bs.getDrawGraphics();
				g.drawImage(image, 0, 0, getWidth(), getHeight(), null);
				g.dispose();

			} while (bs.contentsRestored());

			bs.show();

		} while (bs.contentsLost());
	}
}
