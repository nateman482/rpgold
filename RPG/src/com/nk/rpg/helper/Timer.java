package com.nk.rpg.helper;

public class Timer {

	public long currentTime, startingTime;
	public int length;
	public boolean isRinging = false;
	
	public Timer(long currentTime, int length) {
		this.currentTime = currentTime;
		this.startingTime = currentTime;
		this.length = length;
	}
	
	public void tick(long currentTime) {
		this.currentTime = currentTime;
		if (startingTime - currentTime > length) {
			isRinging = true;
		}
	}
}
