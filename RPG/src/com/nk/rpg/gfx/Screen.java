package com.nk.rpg.gfx;

import java.awt.Font;
import java.awt.Graphics;
import java.awt.image.BufferedImage;

import com.nk.rpg.entity.Entity;

public class Screen {

	public final int width, height;
	public final Graphics g;
	
	private int xoffs, yoffs;
	private final BufferedImage[] tileSprites;
	private final BufferedImage[][] entitySprites;
	
	private String font;
	private int style;
	
	public Screen(int width, int height, Graphics g) {
		this.width = width;
		this.height = height;
		this.g = g;
		
		setFont("Arial", Font.PLAIN, 14);
		
		tileSprites = new SpriteSheet().getSpritesheet("/img/tiles.png");
		entitySprites = new SpriteSheet().get2DSpritesheet("/img/entities.png");
	}
	
	public void setFont(String font, int style, int size) {
		this.font = font;
		this.style = style;
		g.setFont(new Font(font, style, size));
	}
	
	public void setFontSize(int size) {
		g.setFont(new Font(this.font, this.style, size));
	}
	
	public void drawTile(int id, int x, int y) {
		g.drawImage(tileSprites[id], x * 16 - xoffs, y * 16 - yoffs, null);
	}
	
	public void drawEntity(Entity e) {
		int rx = e.x * 16, ry = e.y * 16;
		if (e.moving) {
			if (e.yy != 0) ry = e.y * 16 - e.yy;
			if (e.xx != 0) rx = e.x * 16 - e.xx;
		}
		
		for (int xx = 0; xx < e.id[2]; xx++) {
			for (int yy = 0; yy < e.id[3]; yy++) {
				g.drawImage(entitySprites[xx][yy], rx - xoffs, ry - yoffs, null);
			}
		}
	}
	
	public void write(String msg, int x, int y) {
		g.drawString(msg, x, y);
	}
	
	public void drawPoint(int x, int y) {
		g.fillRect(x, y, 1, 1);
	}
	
	public void drawRect(int x, int y, int w, int h) {
		g.drawRect(x, y, w, h);
	}
	
	public void setOffs(int xoffs, int yoffs) {
		this.xoffs = xoffs;
		this.yoffs = yoffs;
	}
}
