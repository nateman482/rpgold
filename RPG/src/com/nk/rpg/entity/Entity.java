package com.nk.rpg.entity;

import java.util.List;

import com.nk.rpg.Game;

public class Entity {
	
	public final int[] id;
	public int state;
	public int x, y;
	public int xx, yy;
	public int rx, ry;
	public boolean moving = false;
	
	public Entity(int[] id, int state) {
		this.id = id;
		this.state = state;
	}
	
	public void tick(Game game) {
		
	}
	
	public void render(Game game, List<Entity> entities) {
		game.screen.drawEntity(this);
	}
	
	public void calculateCoords() {
		int rx = x * 16, ry = y * 16;
		if (moving) {
			if (yy != 0) ry = y * 16 - yy;
			if (xx != 0) rx = x * 16 - xx;
		}
		
		this.rx = rx;
		this.ry = ry;
	}
	
	public void setState(int state) {
		this.state = state;
	}
	
	public void setLocation(int x, int y) {
		this.x = x;
		this.y = y;
	}
}
