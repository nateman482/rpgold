package com.nk.rpg.entity;

import java.util.List;

import com.nk.rpg.Game;
import com.nk.rpg.state.EntityState;

public class Player extends Entity {

	private static int[] id = {0, 0, 1, 2};
	
	public Player() {
		super(id, EntityState.OVERWORLD.id);
	}
	
	@Override
	public void tick(Game game) {
		int dx = 0, dy = 0;
		if (!moving) {
			if (game.input.downkeys[game.input.controls.get("up")]) dy--;
			if (game.input.downkeys[game.input.controls.get("left")]) dx--;
			if (game.input.downkeys[game.input.controls.get("down")]) dy++;
			if (game.input.downkeys[game.input.controls.get("right")]) dx++;
			if (dx != 0 && dy != 0) dy = 0;
			
			if (!game.overworldMap.collides(id, x, y, dx, dy) && (dx != 0 || dy != 0)) {
				x += dx;
				y += dy;
				xx = dx * 16;
				yy = dy * 16;
				moving = true;
			}
		} if (moving) {
			if (yy < 0) yy++;
			if (xx < 0) xx++;
			if (yy > 0) yy--;
			if (xx > 0) xx--;
			
			if (xx == 0 && yy == 0) moving = false;
		}
	}
	
	public void setOffs(Game game) {
		game.screen.setOffs(rx - game.screen.width / 2 + 8, ry - game.screen.height / 2 + 8);
	}
	
	@Override
	public void render(Game game, List<Entity> entities) {
		
		game.screen.drawEntity(this);
	}
}
