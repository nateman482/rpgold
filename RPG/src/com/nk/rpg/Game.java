package com.nk.rpg;

import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.List;

import com.nk.rpg.entity.Entity;
import com.nk.rpg.entity.Player;
import com.nk.rpg.gfx.Screen;
import com.nk.rpg.input.Input;
import com.nk.rpg.map.OverworldMap;
import com.nk.rpg.state.EntityState;
import com.nk.rpg.state.MenuState;
import com.nk.rpg.state.State;

public class Game {

	public final int mapWidth, mapHeight;
	public final RPG main;
	public final Input input;
	public final Screen screen;
	
	public OverworldMap overworldMap;
	public Player player;
	
	private int state = State.OVERWORLD.id;
	private int menuState = MenuState.MAIN.id;
	private List<Entity> entities, backup, overworldEntities, instanceEntities, cutsceneEntities;
	
	private int MENU_INDEX = 0, TOTAL_MENU_OPTIONS = 3, TOTAL_CONTROLS_MENU_OPTIONS = 5;
	private int lastKey = 0;
	private boolean SELECTING_KEY = false;
	
	public Game(RPG main, Input input, Screen screen, int mapWidth, int mapHeight) {
		this.mapWidth = mapWidth;
		this.mapHeight = mapHeight;
		this.main = main;
		this.input = input;
		this.screen = screen;
		
		initializeLists();
		initializeEntities();
		initializeObjects();
		
		sort();
	}
	
	private void initializeEntities() {
		player = new Player();
		entities.add(player);
//		entities.add(new TestMob(5, 5));
	}
	
	private void initializeObjects() {
		overworldMap = new OverworldMap(mapWidth, mapHeight, player);
	}
	
	private void initializeLists() {
		entities = new ArrayList<Entity>();
		backup = new ArrayList<Entity>();
		overworldEntities = new ArrayList<Entity>();
		instanceEntities = new ArrayList<Entity>();
		cutsceneEntities = new ArrayList<Entity>();
	}
	
	public void tick() {
		input.tick();
		
		backup.clear();
		for (int i = 0; i < entities.size(); i++) {
			backup.add(entities.get(i));
		}
		
		//factor menu logic out at some point
		if (state == State.MENU.id) {
			if (menuState == MenuState.MAIN.id) {
				int dif = 0;
				if (input.togglekeys[KeyEvent.VK_UP]) dif--;
				if (input.togglekeys[KeyEvent.VK_DOWN]) dif++;
				
				MENU_INDEX = getMenuPlace(dif, MENU_INDEX, TOTAL_MENU_OPTIONS);
				
				if (input.togglekeys[KeyEvent.VK_ENTER]) {
					if (MENU_INDEX == 0) {
						state = State.OVERWORLD.id;
					} else if (MENU_INDEX == 1) {
						menuState = MenuState.CONTROLS.id;
						MENU_INDEX = 0;
					} else if (MENU_INDEX == 2) {
						main.stop();
					}
				}
			} else if (menuState == MenuState.CONTROLS.id) {
				if (input.togglekeys[KeyEvent.VK_ENTER]) {
					lastKey = input.lastPressedKey;
					SELECTING_KEY = true;
				}
				
				if (!SELECTING_KEY) {
					if (input.togglekeys[KeyEvent.VK_ESCAPE]) {
						menuState = MenuState.MAIN.id;
						MENU_INDEX = 1;
					}
					
					int dif = 0;
					if (input.togglekeys[KeyEvent.VK_UP]) dif--;
					if (input.togglekeys[KeyEvent.VK_DOWN]) dif++;
					
					MENU_INDEX = getMenuPlace(dif, MENU_INDEX, TOTAL_CONTROLS_MENU_OPTIONS);
				} else {
					if (input.lastPressedKey != this.lastKey) {
						SELECTING_KEY = false;
						if (MENU_INDEX == 0) input.controls.put("up", input.lastPressedKey);
						if (MENU_INDEX == 1) input.controls.put("left", input.lastPressedKey);
						if (MENU_INDEX == 2) input.controls.put("down", input.lastPressedKey);
						if (MENU_INDEX == 3) input.controls.put("right", input.lastPressedKey);
						if (MENU_INDEX == 4) input.controls.put("escape", input.lastPressedKey);
					}
				}
			}
		} else if (state == State.OVERWORLD.id) {
			if (input.togglekeys[input.controls.get("escape")]) state = State.MENU.id;
			
			overworldMap.tick(this, overworldEntities);
		} else if (state == State.COMBAT.id) {
		} else if (state == State.CUTSCENE.id) {
		}
		
		if (entities.size() == backup.size()) {
			for (int i = 0; i < entities.size(); i++) {
				if (entities.get(i) != backup.get(i)) {
					sort();
					return;
				}
			}
		} else {
			sort();
		}
	}
	
	public void draw(Screen screen) {
		if (state == State.MENU.id) {
			int xx = 13, yy = 28, yc = 20, yd = 17;
			if (menuState == MenuState.MAIN.id) {
				screen.setFontSize(18);
				screen.write("RPG", xx, yy);
				
				screen.setFontSize(14);
				screen.write("Play", xx, yy + yc * 1 + yd);
				screen.write("Options", xx, yy + yc * 2 + yd);
				screen.write("Quit", xx, yy + yc * 3 + yd);
				screen.write(">", xx - 10, yy + yc * (MENU_INDEX + 1) + yd);
				
				screen.setFontSize(10);
				screen.write("Arrow keys + enter to navigate", xx, RPG.HEIGHT - 13);
			} else if (menuState == MenuState.CONTROLS.id) {
				yy = 25;
				xx = 9;
				yc = 10;
				yd = 8;
				
				screen.setFontSize(18);
				screen.write("Controls", xx, yy);
				
				screen.setFontSize(10);
				screen.write("Move up = '" + KeyEvent.getKeyText(input.controls.get("up")) + "' key", xx, yy + yc * 1 + yd);
				screen.write("Move left = '" + KeyEvent.getKeyText(input.controls.get("left")) + "' key", xx, yy + yc * 2 + yd);
				screen.write("Move down = '" + KeyEvent.getKeyText(input.controls.get("down")) + "' key", xx, yy + yc * 3 + yd);
				screen.write("Move right = '" + KeyEvent.getKeyText(input.controls.get("right")) + "' key", xx, yy + yc * 4 + yd);
				screen.write("Menu = '" + KeyEvent.getKeyText(input.controls.get("escape")) + "' key", xx, yy + yc * 5 + yd);
				screen.write(">", xx - 7, yy + yc * (MENU_INDEX + 1) + yd);
				screen.write("Enter to select key to change - ESC to return", xx, RPG.HEIGHT - 13);
				
				screen.setFontSize(12);
				if (SELECTING_KEY) screen.write("Press any key", RPG.WIDTH - 95, yc + 7);
			}
		} else if (state == State.OVERWORLD.id) {
			overworldMap.render(this, overworldEntities);
		} else if (state == State.COMBAT.id) {
			for (Entity entity : instanceEntities) {
				entity.render(this, instanceEntities);
			}
		} else if (state == State.CUTSCENE.id) {
			for (Entity entity : cutsceneEntities) {
				entity.render(this, cutsceneEntities);
			}
		}
	}
	
	private int getMenuPlace(int dif, int index, int totalOptions) {
		int newIndex = index + dif;
		if (newIndex < totalOptions) {
			if (newIndex >= 0) {
				index = newIndex;
			} else {
				index = totalOptions - 1;
			}
		} else {
			index = 0;
		}
		return index;
	}
	
	private void sort() {
		backup.clear();
		overworldEntities.clear();
		instanceEntities.clear();
		cutsceneEntities.clear();
		
		for (Entity entity : entities) {
			if (entity.state == EntityState.OVERWORLD.id) {
				overworldEntities.add(entity);
				continue;
			} else if (entity.state == EntityState.COMBAT.id) {
				instanceEntities.add(entity);
				continue;
			} else if (entity.state == EntityState.CUTSCENE.id) {
				cutsceneEntities.add(entity);
			}
		}
	}
	
	public OverworldMap getMap() {
		return overworldMap;
	}
}
