package com.nk.rpg.state;

public enum State {
	MENU(0), OVERWORLD(1), COMBAT(2), CUTSCENE(3);
	
	public int id;
	
	State(int id) {
		this.id = id;
	}
}
