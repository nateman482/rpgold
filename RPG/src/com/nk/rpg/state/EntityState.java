package com.nk.rpg.state;

public enum EntityState {
	OVERWORLD(0), COMBAT(1), CUTSCENE(2);
	
	public int id;
	
	EntityState(int id) {
		this.id = id;
	}
}
