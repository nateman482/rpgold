package com.nk.rpg.state;

public enum MenuState {
	MAIN(0), CONTROLS(1);
	
	public int id;
	
	MenuState(int id) {
		this.id = id;
	}
}
