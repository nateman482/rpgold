package com.nk.rpg.map;

import java.util.List;

import com.nk.rpg.Game;
import com.nk.rpg.entity.Entity;
import com.nk.rpg.entity.Player;
//import com.nk.rpg.map.script.Script;
import com.nk.rpg.map.tile.Tile;

public class OverworldMap {

	public final int width, height;
	public final Tile[][] tiles;
	
	public Player player;
	
	public OverworldMap(int width, int height, Player player) {
		this.width = width;
		this.height = height;
		this.player = player;
		
		tiles = new Tile[width][height];
		
		init();
		
		for (int xx = 0; xx < tiles.length; xx++) {
			for (int yy = 0; yy < tiles[0].length; yy++) {
				tiles[xx][yy] = Tile.ground;
			}
		}
	}
	
	private void init() {
		
	}
	
	public void tick(Game game, List<Entity> entities) {
		player.tick(game);
		player.calculateCoords();
		player.setOffs(game);
		
		for (Entity entity : entities) {
			if (entity.equals(player)) continue;
			entity.tick(game);
			entity.calculateCoords();
		}
	}
	
	public void render(Game game, List<Entity> entities) {
		for (int xx = 0; xx < tiles.length; xx++) {
			for (int yy = 0; yy < tiles[0].length; yy++) {
				game.screen.drawTile(tiles[xx][yy].id, xx, yy);
			}
		}
		
		player.render(game, entities);
		
		for (Entity entity : entities) {
			if (entity.equals(player)) continue;
			entity.render(game, entities);
		}
	}
	
	public boolean collides(int[] id, int x, int y, int xd, int yd) {
		return false;
	}
}
